import * as Joi from '@hapi/joi';

export const signInSchema = Joi.object().keys({
  allowEmail: Joi.boolean().default(false),
  allowAvatar: Joi.boolean().default(false),
  name: Joi.string(),
});
