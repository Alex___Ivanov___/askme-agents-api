import {
  Controller,
  Get,
  Req,
  Res,
  UseGuards,
  Post,
  HttpStatus,
  UseInterceptors,
  Body,
} from '@nestjs/common';
import { AuthService } from './auth.service';
import { OAuthGuard } from './guards/oauth.guard';
import { Provider } from './decorators/auth.decorator';
import { LoggingInterceptor } from './interceptor/logging.interceptor';
import { JoiValidationPipe } from 'src/pipes/validation.pipe';
import { signInSchema } from './validations/sign_up.schema';

@Controller('auth')
export class AuthController {
  constructor(private authService: AuthService) {}

  @Post('google/sign_in')
  @UseGuards(OAuthGuard)
  @Provider('google')
  @UseInterceptors(LoggingInterceptor)
  async signInGoogle(
    @Req() req,
    @Res() res,
    @Body(new JoiValidationPipe(signInSchema)) agentParams,
  ) {
    const { agent, created } = await this.authService.createOAuthAgent(
      req.oAuthAgent,
      agentParams,
    );
    res.status(created ? HttpStatus.CREATED : HttpStatus.OK).json(agent);
  }

  @Post('facebook/sign_in')
  @UseGuards(OAuthGuard)
  @Provider('facebook')
  @UseInterceptors(LoggingInterceptor)
  async signInFacebook(
    @Req() req,
    @Res() res,
    @Body(new JoiValidationPipe(signInSchema)) agentParams,
  ) {
    const { agent, created } = await this.authService.createOAuthAgent(
      req.oAuthAgent,
      agentParams,
    );
    res.status(created ? HttpStatus.CREATED : HttpStatus.OK).json(agent);
  }
}
