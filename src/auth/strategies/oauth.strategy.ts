import * as passport from 'passport';
import { Strategy as GoogleStrategy } from 'passport-google-token';
import * as FacebookStrategy from 'passport-facebook-token';
import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';

@Injectable()
export class OauthStrategy {
  constructor(private configService: ConfigService) {}

  private googleStrategyConfig = {
    clientID: this.configService.get<string>('GOOGLE_CLIENT_ID'),
    clientSecret: this.configService.get<string>('GOOGLE_CLIENT_SECRET'),
  };

  private facebookStrategyConfig = {
    clientID: this.configService.get<string>('FACEBOOK_CLIENT_ID'),
    clientSecret: this.configService.get<string>('FACEBOOK_CLIENT_SECRET'),
  };

  private getSocialFields(accessToken, refreshToken, profile, next) {
    const provider = profile.provider;
    const { id, name, picture, email } = profile._json;
    const avatar = picture || (profile.photos && profile.photos[0].value);

    console.log(profile);

    next({ fields: { alias: name, provider, avatar, id, email } });
  }

  getPassport() {
    passport.use(
      'google',
      new GoogleStrategy(this.googleStrategyConfig, this.getSocialFields),
    );

    passport.use(
      'facebook',
      new FacebookStrategy(this.facebookStrategyConfig, this.getSocialFields),
    );

    return passport;
  }
}
