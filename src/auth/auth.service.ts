import { Injectable, UnauthorizedException } from '@nestjs/common';
import { OauthStrategy } from './strategies/oauth.strategy';
import { AgentsService } from 'src/agents/agents.service';
import { IAgent } from 'src/agents/database/agent.interface';

@Injectable()
export class AuthService {
  private passport;

  constructor(
    private oauthStrategy: OauthStrategy,
    private agentService: AgentsService,
  ) {
    this.passport = oauthStrategy.getPassport();
  }

  /**
   * Check Agent access_token with OAuth, get and return allowed Agent data;
   */
  public async verifyAgentData(req, res, next, provider) {
    return new Promise((resolve, reject) => {
      this.passport.authenticate(provider, data => {
        if (!data || !data.fields) {
          req.session.destroy();
          return reject(
            new UnauthorizedException('Token missing or not valid'),
          );
        }
        return resolve({ ...data.fields });
      })(req, res, next);
    });
  }

  public async createOAuthAgent(
    providerFields,
    agentParams,
  ): Promise<{ agent: IAgent; created: boolean }> {
    const { agent, created } = await this.agentService.createAgent({
      email: agentParams.allowEmail ? providerFields.email : null,
      avatar: agentParams.allowAvatar ? providerFields.avatar : null,
      name: agentParams.name ? agentParams.name : providerFields.alias,
      provider: providerFields.provider,
      providerId: providerFields.id,
    });
    return { agent, created };
  }
}
