import { Module } from '@nestjs/common';
import { AuthController } from './auth.controller';
import { AuthService } from './auth.service';
import { OauthStrategy } from './strategies/oauth.strategy';
import { AgentsModule } from 'src/agents/agents.module';

@Module({
  imports: [AgentsModule],
  controllers: [AuthController],
  providers: [AuthService, OauthStrategy],
})
export class AuthModule {}
