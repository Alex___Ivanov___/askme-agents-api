import { Injectable, CanActivate, ExecutionContext } from '@nestjs/common';
import { AuthService } from '../auth.service';
import { Reflector } from '@nestjs/core';

@Injectable()
export class OAuthGuard implements CanActivate {
  constructor(private authService: AuthService, private reflector: Reflector) {}

  async canActivate(context: ExecutionContext): Promise<boolean> {
    const req = context.switchToHttp().getRequest();
    const res = context.switchToHttp().getResponse();
    const next = context.switchToHttp().getNext();
    const provider = this.reflector.get<string>(
      'provider',
      context.getHandler(),
    );

    req.oAuthAgent = await this.authService.verifyAgentData(
      req,
      res,
      next,
      provider,
    );

    req.session.agentProvider = req.oAuthAgent.provider;
    req.session.agentProviderId = req.oAuthAgent.id;

    return req.oAuthAgent;
  }
}
