import { Injectable, CanActivate, ExecutionContext } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { AgentsService } from 'src/agents/agents.service';

@Injectable()
export class SessionGuard implements CanActivate {
  constructor(
    private reflector: Reflector,
    private agentsService: AgentsService,
  ) {}

  async canActivate(context: ExecutionContext): Promise<boolean> {
    const req = context.switchToHttp().getRequest();

    const { agentProvider, agentProviderId } = req.session;

    const agent = await this.agentsService.getOneByProvider(
      agentProvider,
      agentProviderId,
    );

    req.agent = agent;

    return req.agent;
  }
}
