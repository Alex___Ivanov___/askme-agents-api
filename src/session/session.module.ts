import * as ConnectRedis from 'connect-redis';
import * as session from 'express-session';
import { RedisService } from 'nestjs-redis';
import { NestSessionOptions, SessionModule } from 'nestjs-session';
import { Redis } from 'src/redis/redis.module';
import { ConfigModule, ConfigService } from '@nestjs/config';

const RedisStore = ConnectRedis(session);

export const Session = SessionModule.forRootAsync({
  imports: [Redis, ConfigModule],
  inject: [RedisService, ConfigService],
  useFactory: (
    redisService: RedisService,
    config: ConfigService,
  ): NestSessionOptions => {
    const redisClient = redisService.getClient();
    const store = new RedisStore({ client: redisClient as any });
    return {
      session: {
        store,
        secret: config.get<string>('SESSION_SECRET'),
        cookie: { maxAge: 8.64e7 },
        name: 'connect.sid',
        resave: false,
        saveUninitialized: true,
      },
    };
  },
});
