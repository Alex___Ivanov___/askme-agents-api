import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.setGlobalPrefix('api');

  const options = new DocumentBuilder()
    .setTitle('AskMe REST Docs')
    .setDescription('REST docs for api AskMe')
    .setVersion('1.0')
    .addTag('users')
    .addTag('tags')
    .addTag('auth')
    .build();
  const document = SwaggerModule.createDocument(app, options);
  SwaggerModule.setup('api/docs', app, document);

  const PORT = parseInt(process.env.PORT, 10) || 3000;

  await app.listen(PORT);
}
bootstrap();
