import {
  Injectable,
  NotFoundException,
  BadRequestException,
} from '@nestjs/common';
import { AgentRepository } from './database/agent.repository';
import { IAgent } from './database/agent.interface';
import { CreateAgentDto } from './dto/create-agent.dto';

@Injectable()
export class AgentsService {
  constructor(private agentRepository: AgentRepository) {}

  async getOne(name: string): Promise<IAgent> {
    const agent = await this.agentRepository.getOne(name);

    if (!agent) throw new NotFoundException(`Agent ${name} not found!`);
    return agent;
  }

  async getOneByProvider(
    provider: string,
    providerId: string,
  ): Promise<IAgent> {
    const agent = await this.agentRepository.getOneByCond({
      providerId,
      provider,
    });

    if (!agent) throw new NotFoundException(`Agent not found!`);
    return agent;
  }

  async getAll({limit, skip}): Promise<IAgent[]> {
    return this.agentRepository.getAll(limit, skip);
  }

  async createAgent(
    agent: CreateAgentDto,
  ): Promise<{ agent: IAgent; created: boolean }> {
    const exist = await this.agentRepository.getOneByCond({
      provider: agent.provider,
      providerId: agent.providerId,
    });
    if (exist) {
      return { agent: exist, created: false };
    }
    return {
      agent: await this.agentRepository.createAgent(agent),
      created: true,
    };
  }

  async validateId(
    agentId: string,
    providerId: string,
    provider: string,
  ): Promise<IAgent> {
    const agent = await this.agentRepository.getOneByCond({
      providerId,
      provider,
    });
    if (!agent)
      throw new BadRequestException('Not found agent for current session!');
    if (agent.id !== agentId) {
      throw new BadRequestException(
        'Incorrect agent ID for current session!',
      );
    }
    return agent;
  }
}
