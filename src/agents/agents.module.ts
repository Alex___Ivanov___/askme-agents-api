import { Module } from '@nestjs/common';
import { AgentsService } from './agents.service';
import { AgentsController } from './agents.controller';
import { AgentRepository } from './database/agent.repository';
import { DatabaseModule } from 'src/database/database.module';
import { agentsProviders } from './agents.providers';

@Module({
  imports: [DatabaseModule],
  providers: [AgentsService, AgentRepository, ...agentsProviders],
  controllers: [AgentsController],
  exports: [AgentsService],
})
export class AgentsModule {}
