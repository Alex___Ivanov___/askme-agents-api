import {
  Controller,
  Get,
  Param,
  Req,
  Session,
  UseGuards,
  Query,
} from '@nestjs/common';
import { AgentsService } from './agents.service';
import { IAgent } from './database/agent.interface';
import { SessionGuard } from 'src/auth/guards/session.guard';
import { JoiValidationPipe } from 'src/pipes/validation.pipe';
import { getAllSchema } from './validations/getAll.schema';

@Controller('agents')
export class AgentsController {
  constructor(private agentService: AgentsService) {}

  @Get('me')
  @UseGuards(SessionGuard)
  async me(@Session() session) {
    return this.agentService.getOneByProvider(
      session.agentProvider,
      session.agentProviderId,
    );
  }

  @Get('validate_id')
  @UseGuards(SessionGuard)
  async validateMyself(@Req() req, @Session() session) {
    return this.agentService.validateId(
      req.query.id,
      session.agentProviderId,
      session.agentProvider,
    );
  }

  @Get('')
  async getAllAgent(@Query(new JoiValidationPipe(getAllSchema)) params) {
    return this.agentService.getAll(params);
  }

  @Get(':name')
  async getOneAgent(@Param() params): Promise<IAgent> {
    return this.agentService.getOne(params.name);
  }
}
