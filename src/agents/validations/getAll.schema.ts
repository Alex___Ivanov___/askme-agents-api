import * as Joi from '@hapi/joi';

export const getAllSchema = Joi.object().keys({
  limit: Joi.number()
    .integer()
    .positive()
    .default(20),
  skip: Joi.number()
    .integer()
    .positive()
    .allow(0)
    .default(0),
});
