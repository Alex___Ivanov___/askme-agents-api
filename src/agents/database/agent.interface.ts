import * as mongoose from 'mongoose';

export interface IAgent extends mongoose.Document {
  readonly name: string;
  readonly alias: string;
  readonly email: string;
  readonly socialId: string;
  readonly socialProvider: string;
  readonly avatar: string;
}
