import * as mongoose from 'mongoose';

const Schema = new mongoose.Schema(
  {
    name: { type: String, required: true },
    email: { type: String, required: false, default: null },
    providerId: { type: String, required: true },
    provider: { type: String, required: true },
    avatar: { type: String, required: false, default: null },
  },
  { versionKey: false },
);

// Ensure virtual fields are serialised.
Schema.set('toJSON', {
  virtuals: true,
  getters: true,
});

Schema.index({ providerId: 1, provider: 1 }, { unique: true });

export const AgentSchema = Schema;
