import { Model } from 'mongoose';
import { Injectable, Inject } from '@nestjs/common';
import { IAgent } from './agent.interface';
import { CreateAgentDto } from '../dto/create-agent.dto';

@Injectable()
export class AgentRepository {
  constructor(@Inject('AGENT_MODEL') private agentModel: Model<IAgent>) {}

  async getOne(name: string): Promise<IAgent> {
    return this.agentModel.findOne({ name }).exec();
  }

  async getOneByCond(cond): Promise<IAgent> {
    return this.agentModel.findOne(cond).exec();
  }

  async getAll(limit: number, skip: number): Promise<IAgent[]> {
    return this.agentModel
      .find()
      .sort({ _id: -1 })
      .skip(skip)
      .limit(limit)
      .exec();
  }

  async createAgent(agentData: CreateAgentDto): Promise<IAgent> {
    const agent = new this.agentModel(agentData);
    return agent.save();
  }
}
