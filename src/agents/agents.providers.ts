import { Mongoose } from 'mongoose';
import { AgentSchema } from './database/agent.schema';

export const agentsProviders = [
  {
    provide: 'AGENT_MODEL',
    useFactory: (mongoose: Mongoose) => mongoose.model('Agent', AgentSchema),
    inject: ['DATABASE_CONNECTION'],
  },
];