import { IsString, IsEmail, IsUrl } from 'class-validator';

export class CreateAgentDto {
  @IsString()
  name: string;

  @IsEmail()
  email: string;

  @IsUrl()
  avatar: string;

  @IsString()
  providerId: string;

  @IsString()
  provider: string;
}
