import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { AuthModule } from './auth/auth.module';
import { AgentsModule } from './agents/agents.module';
import { Redis } from './redis/redis.module';
import { Session } from './session/session.module';

@Module({
  imports: [
    AuthModule,
    AgentsModule,
    ConfigModule.forRoot({
      isGlobal: true,
    }),
    Redis,
    Session,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
